package main

import (
	"flag"
	"fmt"
	"strconv"
)

var length int
var tickscount int
var tcount int
var buckets []int
var ticks []int
var currentposition int

var maxv int

func check() bool {
	var t = 0
	for i := 0; i < length; i++ {
		if buckets[i] >= maxv {
			fmt.Println(buckets)
			return false
		}
	}

	for i := 0; i < length; i++ {
		for j := i + 1; j < length; j++ {
			k := buckets[j] - buckets[i]
			if k < 0 {
				currentposition = j
				return false
			}
			ticks[t] = k
			t++
			if t > 1 {
				for g := 0; g < t-1; g++ {
					if ticks[g] == k {
						currentposition = j
						return false
					}
				}
			}
		}
	}
	return true
}

func main() {

	lengthFlag := flag.Int("l", 6, "Golomb Length")
	flag.Parse()
	length = *lengthFlag
	tickscount = (length * (length - 1) / 2)
	buckets = make([]int, length)
	ticks = make([]int, tickscount)

	currentposition = length - 1
	tcount = tickscount - 1
	maxv = length * length

	var terminate = false

	for i := 0; i < length; i++ {
		buckets[i] = i
	}

	for {
		for {
			r := check()
			//fmt.Println("r: ",r)
			if r {
				break
			}
			if currentposition == 0 {
				break
			}
			if !r {
				buckets[length-1]++

				for j := length - 1; j > 0; j-- {
					if buckets[j] > maxv {
						if j == 1 {
							terminate = true
							break
						}
						buckets[j] = buckets[j-1] + 2
						buckets[j-1]++
						if buckets[1] == maxv {
							terminate = true
							break
						}
					}
				}
				if terminate {
					break
				}
			}
			if terminate {
				break
			}
		}
		if terminate {
			break
		}
		if currentposition == 0 {
			break
		}
		fmt.Print("Found: ")
		//fmt.Println(buckets)
		for i := 0; i < length; i++ {
			fmt.Print(strconv.Itoa(buckets[i]) + " ")
		}
		fmt.Println()
		maxv = buckets[length-1]
		fmt.Println("maxV:", strconv.Itoa(maxv))
	}
	fmt.Println("Done")

}
